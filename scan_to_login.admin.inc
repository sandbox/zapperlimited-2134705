<?php
/**
 * @file
 * A module that allows you to scan a QR code to register 
 * and log in on a drupal site.
 * 
 * Created by ZapGroup.
 *
 * @package scan_to_login.module
 */

/**
 * Adds fields to the Scan to Login configuration landing page.
 */
function scan_to_login_form() {

  $form = array(
    'scan_to_login_merchant_id' => array(
      '#type' => 'textfield',
      '#title' => t('Merchant ID'),
      '#default_value' => variable_get('scan_to_login_merchant_id', 0),
      '#size' => 20,
      '#maxlength' => 20,
      '#description' => t('The Merchant ID from your Zapper account'),
      '#required' => TRUE,
    ),
    'scan_to_login_site_id' => array(
      '#type' => 'textfield',
      '#title' => t('Site ID'),
      '#default_value' => variable_get('scan_to_login_site_id', 0),
      '#size' => 20,
      '#maxlength' => 20,
      '#description' => t('The Site ID from your Zapper account'),
      '#required' => TRUE,
    ),
    'scan_to_login_allow_user_registration' => array(
      '#type' => 'checkbox',
      '#title' => t('Allow user registration'),
      '#default_value'
      => variable_get('scan_to_login_allow_user_registration', 0),
      '#description' => t('Allow user registration through the Zapper QR code'),
    ),
  );

  return system_settings_form($form);
}
