<?php
/**
 * @file
 * Scan to Login Helper Class
 * Group of related methods that manipulate the current drupal authentication.
 *
 * @package ScanToLoginHelper
 */

class ScanToLoginHelper {

  /**
   * Authorise a user programmatically through the drupal "api".
   */
  public function authoriseUser($username, $password) {

    $response = array(
      'errors' => array(),
      'success' => FALSE,
      'username' => $username,
      'password' => $password,
    );

    if (user_authenticate($username, $password)) {

      $user_obj = user_load_by_name($username);

      $form_state = array();
      $form_state['uid'] = $user_obj->uid;

      user_login_submit(array(), $form_state);

      $response['success'] = TRUE;
    }
    else {
      $response['errors'] = 'User authentication failed';
    }

    drupal_json_output($response); exit;
  }

  /**
   * Register a user programmatically through the drupal "api".
   */
  public function registerUser($email, $password) {

    $response = array(
      'errors' => array(),
      'success' => FALSE,
      'username' => $email,
      'password' => $password,
    );

    if (empty($email)) {
      $response['errors'][] = 'Please enter an email address';
    }
    if (empty($password)) {
      $response['errors'][] = 'Please enter a password';
    }

    // Check if user is already registered.
    $user_obj = user_load_by_name($email);

    if (!empty($user_obj->uid)) {
      $response['errors'][] = 'This email address has already been registered';
    }

    if (empty($response['errors'])) {

      $user = array(
        'name' => $email,
        'pass' => $password,
        'mail' => $email,
        'status' => 1,
        'init' => $email,
      );

      $user_obj = user_save(NULL, $user);

      if (!empty($user_obj->uid)) {
        $response['success'] = TRUE;
      }
    }

    drupal_json_output($response); exit;
  }
}
