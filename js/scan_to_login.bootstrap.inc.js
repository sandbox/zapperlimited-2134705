/**
* @file
* Scan-to-Login for Drupal.
*
* Created: June 2013
* Creator: ZapGroup
* Website: http://www.zapper.com/
* API Version: 1.1.60
*/

(function($){

  $(function() {

    var merchantId = Drupal.settings.scan_to_login.merchantId;
    var siteId = Drupal.settings.scan_to_login.siteId;
    var allowUserRegistration = Drupal.settings.scan_to_login.allowUserRegistration;
    var apiUrl = Drupal.settings.scan_to_login.apiUrl;
    var baseUrl = Drupal.settings.scan_to_login.baseUrl;
    var placeHolder = $('<div id="scantologin-container-horizontal"><ul><li id="scantologin-logo"></li><li id="scantologin-barcode"></li></ul></div>');
    var placeHolderEnd = $('<div id="scantologin-container-horizontal-end"><span id="scantologin-available-for"></span><a href="http://www.zapper.com/" target="_blank" id="scantologin-zapper-link">www.zapper.com</a></div>');

    if (merchantId && siteId) {

      if ($('#user-login-form').length > 0) {
        var placeHolder = $('<div id="scantologin-container"><div id="logo-container" class="zapperLogo"></div><div class="scantologin-qrcode-placeholder"></div><div id="scantologin-end-container"><span id="scantologin-available-for"></span><a href="http://www.zapper.com/" target="_blank" id="scantologin-zapper-link">www.zapper.com</a></div></div>');
        $('#user-login-form').prepend(placeHolder);
      }

      if ($('#user-login').length > 0 || $('#user-register-form').length > 0) {
        $('div.content').css('position', 'relative');
        $('#user-login').prepend(placeHolder);
        $('#user-login').append(placeHolderEnd);
        $('.form-item').css({
            'position': 'absolute',
            'left': '339px',
            'width': '395px'
        });
        $('.form-item.form-item-username').css('top', '0');
        $('.form-item.form-item-pass').css('top', '77px');
        $('#edit-actions').css({
          'top': '132px',
          'left': '335px',
          'position': 'absolute'
        });
        $('input.form-text').css('width', '375px');
      }

      if ($('#user-register-form').length > 0) {
        $('div.content').css('position', 'relative');
        $('#user-register-form').prepend(placeHolder);
        $('#user-register-form').append(placeHolderEnd);
        $('.form-item').css({
          'position': 'absolute',
          'left': '339px',
          'width': '390px'
        });
        $('.form-item.form-item-username').css('top', '0');
        $('.form-item.form-item-mail').css('top', '95px');
        $('#edit-actions').css({
            'top': '210px',
            'left': '335px',
            'position': 'absolute'
        });
        $('input.form-text').css('width', '375px');
      }

      var qrCode = new ZapperTech.QrCode({
        merchantId: merchantId,
        siteId: siteId,
        selector: placeHolder,
        baseUrl: apiUrl
      });

      qrCode.registrationRequest(function(data){
        if(!allowUserRegistration) {
          var error = 'This system does not allow self registration.';
          alert(error);
          qrCode.registrationRespond({
            success: false,
            errors: [error],
            username: '',
            password: ''
          });
        } else {
          var email = data.getAnswer(qrCode.QUESTIONTYPE.email);
          var firstName = data.getAnswer(qrCode.QUESTIONTYPE.firstName);
          var lastName = data.getAnswer(qrCode.QUESTIONTYPE.lastName);
          var password = data.Password;
          $.ajax({
            url: baseUrl + '?zappertech=register',
            type: 'POST',
            data: {
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password
            },
            dataType: 'json'
          }).done(function(result){
            var response = {
              success: result.success,
              errors: result.errors,
              username: result.username,
              password: result.password
            };
            qrCode.registrationRespond(response);
            if(result.success === false) {
              var alertMessage = "Registration failed: \n";
              $(response.errors).each(function(i, error) {
                alertMessage += error + "\n";
              });
              alert(alertMessage);
            } else {
              login({
                Username: result.username,
                Password: result.password
              });
            }
          });
        }
      });

      var login = function(data){
        $.ajax({
          url: baseUrl + '?zappertech=authenticate',
          type: 'POST',
          data: {
            username: data.Username,
            password: data.Password
          },
          dataType: 'json',
          complete: function(result){
            result = eval('(' + result.responseText + ')');
            var response = {
              success: result.success,
              errors: result.errors,
              username: result.username,
              password: result.password
            };
            qrCode.loginRespond(response);
            window.setTimeout(function() {
              if ($('#user-register-form').length > 0) {
                location.href = baseUrl + '?q=user';
              } else {
                if ($('form#user-login-form').length > 0) {
                  $form = $('form#user-login-form').clone();
                } else {
                  $form = $('form#user-login').clone();
                }
                $('#edit-name', $form).val(response.username);
                $('#edit-pass', $form).val(response.password);
                $form.submit();
                window.location.href = baseUrl + '?q=user';
              }
            }, 1000);
          }
        });
      };
      qrCode.loginRequest(login);
      qrCode.start();
    }
  });
})(jQuery);
